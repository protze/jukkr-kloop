echo
echo "start building kloop mini-app"
echo "============================================================"
echo

set +e

mkdir -p build_kloop_miniapp_base/
cd build_kloop_miniapp_base/ && FC=ifort cmake \
  -DCMAKE_BUILD_TYPE=Release \
  -DENABLE_DEBUG_SYMBOLS=ON \
  -DENABLE_MPI=OFF \
  -DENABLE_OMP=ON \
  -DENABLE_OMP_EVERYWHERE=ON \
  -DENABLE_OPTRPT=OFF \
  -DOPTFLAGS_AVX512=OFF\
  -DOPTFLAGS_xHOST=ON \
  -DTIMING_DETAIL=ON \
  .. \
  && make -j # VERBOSE=1

ln -s ../input/input_kloop_ept_* .

echo "finished building the kloop mini-app"