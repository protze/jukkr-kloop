# juKKR-kloop kernel
This repository contains a miniapp reproducing the k-point integration of the KKRhost code which is part of the Juelich KKR code family.
The k-point integration is an important step in solving the algebraic Dyson equation which is the most important hotspot of the KKRhost code.
For each k-point the inverse scattering path operator needs to be computed.
In fact this is a matrix inversion which is numerically done by computing a LU decomposition of the scattering path operator.

The kernel comes in two versions which can be found on the *master* and *openmp* branch respectively.  
In the first version (*master*) the loop over individual k-points is **not** parallelized.
Instead parallelization occurs only when computing the LU decomposition of the scattering path operator by using the *threaded* version of the Intel MKL.  
In the second version (*openmp*) the loop over individual k-points is parallelized using OpenMP and the LU decomposition of the scattering path operator is computed using the *serial* version of the Intel MKL.


## How to build the kernel
The kernel provides a script `compile_kloop.sh` as a convenient way to build the kernel.
This script will create a build directory called `./build_kloop_miniapp` and start the build process using the CMake toolchain.  
**Note:** the compile script sets the Fortran compiler to the Intel Fortran compiler `ifort`.
The kernel has only been tested using the Intel Fortran compiler version `19.0.1.144`.


## How to run the kernel
After the build process has been completed you will find an executable called `test_kloop.x` inside the build directory `./build_kloop_miniapp`.
You can run the code using the following command:
```
OMP_STACKSIZE=5G OMP_PLACES=cores OMP_PROC_BIND=close OMP_NUM_THREADS=<nthreads> 
```
You can choose different numbers of OpenMP threads by varying `<nthreads>`.  
**Important:** if you do not set `OMP_STACKSIZE` to a large enough value (e.g. `5G`) you will encounter **segmentation faults**!